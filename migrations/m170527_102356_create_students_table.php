<?php

use yii\db\Migration;

/**
 * Handles the creation of table `students`.
 */
class m170527_102356_create_students_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('students', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'id_num'  => $this->string(),
            'age' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('students');
    }
}
